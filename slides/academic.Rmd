---
title: Example of Conference Presentation
author: 
    -"Ana-Maria Gore"
    -"Olga González-Arribas"
date: "ana-maria.gore@student.uva.nl"
output:
    revealjs::revealjs_presentation:
        css: "styles.css"
        theme: "white"
        incremental: true
---

# Outline

1. Introduction
2. Literature Review
3. Research Question
4. Methods
5. Analysis
6. Results
7. Discussion

---

# Introduction

•  Purpose
•  Definitions
•  Background
•  Goals

---

# Literature Review

- Study 1
- Study 2

---

# Research Question

> "What is the meaning of life?" 

---

# Methods

- Go/No-Go/Choose task
- Discrimination task

---

### Hypotheses
1. H1
2. H2
---

# Analyses

### Main analysis

### Exploratory analysis


# Results

- | test | p-val |
| ------ | ------ |
|        |        |
|        |        |

# Discussion

- Conclusions
- Limitations

# References

